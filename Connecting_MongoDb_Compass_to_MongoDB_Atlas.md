- [References](#references)
- [Connecting MongoDb Compass to MongoDB Atlas](#connecting-mongodb-compass-to-mongodb-atlas)
  - [Install Compass](#install-compass)
  - [Create Atlas Account](#create-atlas-account)
  - [Create Cluster](#create-cluster)
  - [Create Database & Network Access](#create-database--network-access)
    - [Database Access](#database-access)
    - [Nextwork Access](#nextwork-access)
    - [Data Setup](#data-setup)
    - [Setting up MongoDB URI](#setting-up-mongodb-uri)
  - [Connect Cluster to Compass](#connect-cluster-to-compass)

# References
- https://www.youtube.com/watch?v=vAHd7oV1uE0&ab_channel=devn8

# Connecting MongoDb Compass to MongoDB Atlas

## Install Compass
  - Download : https://www.mongodb.com/try/download/compass

## Create Atlas Account
  Sign in @ https://www.mongodb.com/.

## Create Cluster
  Once signed in hit the "+ Create" button

  <img src="./images/mongo-db-create-cluster-01.png" />

  Then selected "Shared" to create a shared cluster. This is the free version. Select your cloud provider and region. If you wish to name your cluster go to the next step, else go ahead and create the cluster.

  <img src="./images/mongo-db-create-cluster-02.png" />

  It's important to name your cluster now if that matters to you. You won't be able to change it once it has been created.

  <img src="./images/mongo-db-create-cluster-03.png" />

## Create Database & Network Access
  We're going to be using the two tabs indicated below.
  - Database Access
  - Network Access

  ### Database Access

  <img src="./images/mongo-db-create-cluster-04.png" />
  
  Create a new Database user by clicking, "Add New Database User".

  <img src="./images/mongo-db-create-cluster-05.png" />

  Make sure that you're on the "Password" tab. Enter in a Username and Password. These will be used later on.

  <img src="./images/mongo-db-create-cluster-06.png" />

  Next select the role you wish to use and create.

  <img src="./images/mongo-db-create-cluster-7-x.png" />

  Extra info about the permisions of the roles

  <img src="./images/mongo-db-create-cluster-07.png" />

  Here's the finished result

  <img src="./images/mongo-db-create-cluster-08.png" />

  ### Nextwork Access
  Navigate to the Network access tab that I showed above. Click on, "Add IP Address".

  <img src="./images/mongo-db-create-cluster-09.png" />

  Allow access from anywhere and confirm.

  <img src="./images/mongo-db-create-cluster-10.png" />

  ### Data Setup
  Navigate to the database tab. Click on the "Browse Collections" tab.

  <img src="./images/mongo-db-create-cluster-12.png" />

  Make sure you're on the Collections tab within the Collections page. Click, "Load a Sample Dataset".

  <img src="./images/mongo-db-create-cluster-13.png" />

  Select, "Load Sample Dataset". This may take a while for all the data to load. This will create databases. ~~You'll need to reference one of these databases in your MongoDB URI. Once, the data is finished loading you'll simply go back to the collections tab. You'll then see all the databases created.~~

  <img src="./images/mongo-db-create-cluster-14.png" />

  ### Setting up MongoDB URI
  Click on the Database tab. Next, click the "connect" button.

  <img src="./images/mongo-db-create-cluster-15.png" />
  
  Select, "Connect using MongoDB Compass".
  
  <img src="./images/mongo-db-create-cluster-16.png" />
  
  Select, "I have MongoDB Compass" and Copy the connection string.
  
  > Note : You only need to change `<password>` to your database user password and `test` can be removed. ~~you need to change `<password>` to your database user password and `test` at the end of the string to the database you wish to use.~~

  ```sh
  # original string
  mongodb+srv://Swift-Magic:<password>@cluster0.ruveakh.mongodb.net/test

  # updated string
  mongodb+srv://Swift-Magic:2tSr8x8vklXmF2R3@cluster0.ruveakh.mongodb.net/
  ```

  <img src="./images/mongo-db-create-cluster-17.png" />

## Connect Cluster to Compass
  The last thing we need to do is open MongoDB compass. Paste the updated URI string into the URI text box and click, "Save & Connect".

  <img src="./images/mongo-db-create-cluster-18.png" />
  
  Enter in the name of the connect, select a color you want, and click, "Save & Connect". You're done! It should now connect the data in your MongoDB Atlas to your MongoDB Compass!

  <img src="./images/mongo-db-create-cluster-19.png" />
